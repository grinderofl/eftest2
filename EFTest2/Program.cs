﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EFTest2
{
    class Program
    {
        static void Main(string[] args)
        {
            var sqlconnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            var builder = new DbModelBuilder(DbModelBuilderVersion.Latest);
            var items = builder.Entity<Item>().Map(x => x.ToTable("Items"));
            var conn = builder.Build(sqlconnection);

            var context = new MyContext();//new DbContext(sqlconnection, conn.Compile(), true);

            var conf = new DbMigrationsConfiguration()
            {
                ContextType = context.GetType(),
                MigrationsAssembly = Assembly.GetCallingAssembly()
            };

            var dbmigrator = new DbMigrator(conf);
            
            dbmigrator.Update();

            context.Set<Item>().Add(new Item() {Name = "Item1"});
            context.SaveChanges();
        }

    }

    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Related> Relateds { get; set; }
    }

    public class Related
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Item Item { get; set; }
    }

    public class MyContext : DbContext
    {
        public MyContext() : base("DefaultConnection")
        {
            
        }
    }
}
